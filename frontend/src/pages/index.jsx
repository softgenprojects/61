import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, Image, Flex, Link, AspectRatio, SimpleGrid } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { DashboardLayout } from '@components/DashboardLayout';
import { MetricsCard } from '@components/MetricsCard';
import { useDashboardData } from '@hooks/useDashboardData';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');
  const { data } = useDashboardData();

  return (
    <DashboardLayout>
      <Box bg={bgColor} color={textColor} minH="100vh">
        <Center py="12" px={{ base: '4', lg: '8' }}>
          <VStack spacing="8">
            <BrandLogo />
            <SimpleGrid columns={{ base: 1, md: 3 }} spacing="40px">
              {data.metrics.length > 0 && data.metrics.map((metric) => (
                <MetricsCard key={metric.name} metricName={metric.name} value={metric.value} />
              ))}
            </SimpleGrid>
          </VStack>
        </Center>
      </Box>
    </DashboardLayout>
  );
};

export default Home;