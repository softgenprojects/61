import { Box, SimpleGrid, Text } from '@chakra-ui/react';
import { DashboardLayout } from '@components/DashboardLayout';
import { FinancialChart } from '@components/FinancialChart';
import { MetricsCard } from '@components/MetricsCard';
import { useDashboardData } from '@hooks/useDashboardData';

export default function DashboardPage() {
  const { data, status } = useDashboardData();

  if (status === 'loading') {
    return (
      <Box p={5}>
        <Text>Loading...</Text>
      </Box>
    );
  }

  if (status === 'error') {
    return (
      <Box p={5}>
        <Text>Error loading data.</Text>
      </Box>
    );
  }

  return (
    <DashboardLayout>
      <Box>
        {data.chartData.length > 0 && <FinancialChart data={data.chartData} config={data.chartConfig} />}
        {data.metrics.length > 0 && (
          <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={5} mt={10}>
            {data.metrics.map((metric) => (
              <MetricsCard key={metric.name} metricName={metric.name} value={metric.value} />
            ))}
          </SimpleGrid>
        )}
      </Box>
    </DashboardLayout>
  );
}