import React from 'react';
import { Box, Text, Stat, StatLabel, StatNumber, useColorModeValue } from '@chakra-ui/react';

export const MetricsCard = ({ metricName, value }) => {
  const cardBg = useColorModeValue('gray.100', 'gray.700');
  const textColor = useColorModeValue('gray.800', 'white');

  return (
    <Box p={5} shadow='md' borderWidth='1px' borderRadius='lg' bg={cardBg}>
      <Stat>
        <StatLabel color={textColor} fontSize='lg'>{metricName}</StatLabel>
        <StatNumber color={textColor} fontSize='2xl'>{value}</StatNumber>
      </Stat>
    </Box>
  );
};