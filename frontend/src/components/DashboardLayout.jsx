import { Box, Flex, Link, VStack, Text, Icon, useBreakpointValue } from '@chakra-ui/react';
import { FiHome, FiTrendingUp, FiCompass, FiStar, FiSettings } from 'react-icons/fi';

export const DashboardLayout = ({ children }) => {
  const sidebarWidth = useBreakpointValue({ base: '0', md: '60' });

  const SidebarItem = ({ icon, children }) => (
    <Link display='flex' alignItems='center' p='4' borderRadius='md' _hover={{ bg: 'gray.100' }}>
      <Icon as={icon} mr='4' />
      <Text>{children}</Text>
    </Link>
  );

  return (
    <Flex h='100vh'>
      <Box w={{ base: sidebarWidth, md: '20%' }} bg='gray.800' color='white' p='5'>
        <VStack align='stretch' spacing='5'>
          <SidebarItem icon={FiHome}>Dashboard</SidebarItem>
          <SidebarItem icon={FiTrendingUp}>Investments</SidebarItem>
          <SidebarItem icon={FiCompass}>Explore</SidebarItem>
          <SidebarItem icon={FiStar}>Favorites</SidebarItem>
          <SidebarItem icon={FiSettings}>Settings</SidebarItem>
        </VStack>
      </Box>
      <Box flex='1' p='8'>
        {children}
      </Box>
    </Flex>
  );
};