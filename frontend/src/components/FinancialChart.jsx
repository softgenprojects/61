import React from 'react';
import { Box, Flex, Text, Heading } from '@chakra-ui/react';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

export const FinancialChart = ({ data, config }) => {
  return (
    <Box p={4} bgGradient='linear(to-r, teal.500, green.500)' borderRadius='lg'>
      <Flex direction='column' align='center' justify='center'>
        <Heading mb={4} color='white'>Investors Dashboard</Heading>
        <Text color='gray.200' mb={6}>Visualizing Financial Data</Text>
        <Box width='100%' height='400px'>
          <ResponsiveContainer width='100%' height='100%'>
            <LineChart data={data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
              <CartesianGrid strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis />
              <Tooltip />
              <Legend />
              {config.lines.map((line) => (
                <Line type='monotone' dataKey={line.dataKey} stroke={line.color} key={line.dataKey} />
              ))}
            </LineChart>
          </ResponsiveContainer>
        </Box>
      </Flex>
    </Box>
  );
};