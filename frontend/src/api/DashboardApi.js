export const fetchDashboardData = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        totalInvestment: '1,000,000',
        currentValue: '1,500,000',
        growthRate: '50%',
        projects: [
          { id: 1, name: 'Project A', investment: '500,000', currentValue: '750,000', growthRate: '50%' },
          { id: 2, name: 'Project B', investment: '300,000', currentValue: '450,000', growthRate: '50%' },
          { id: 3, name: 'Project C', investment: '200,000', currentValue: '300,000', growthRate: '50%' }
        ],
        metrics: [
          { name: 'Total Investment', value: '1,000,000' },
          { name: 'Current Value', value: '1,500,000' },
          { name: 'Growth Rate', value: '50%' }
        ],
        chartData: []
      });
    }, 2000);
  });
};