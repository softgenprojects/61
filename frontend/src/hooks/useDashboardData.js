import { useQuery } from 'react-query';
import { fetchDashboardData } from '@api/DashboardApi';

export const useDashboardData = () => {
  const { data = { metrics: [], chartData: [] }, status } = useQuery('dashboardData', fetchDashboardData);

  return { data, status };
};