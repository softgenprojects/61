import { format } from 'date-fns';

export const FormatDate = (date) => {
  return format(date, 'MMM dd, yyyy');
};