export const FormatNumber = (number, type) => {
  switch (type) {
    case 'currency':
      return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number);
    case 'percentage':
      return `${number}%`;
    default:
      return new Intl.NumberFormat().format(number);
  }
};